# Gentoo Installation Helper Script

Used to provide good defaults to Gentoo to install with or without encryption, hibernation support, and with BtrFS subvolumes.

```
Usage: gentoo-install.sh <disk> [<options>...]

Positional Arguments:
<drive>   Disk device used for system

Options:
-h, --help        Help on this tool.
-e, --encryption  Enable LUKS encryption.
-c, --compression Enable BtrFS compression.
-s, --swap        Enable Swap/Hibernation support.
-d, --debug       Enable DEBUG mode for testing.

--stage <stage>   Installation using stagefile <stage>, for stage3 or stage4
--clean           Cleanup disk for clean slate
```

## Initial Setup

This installation script is intended to work on a clean disk to provision all partitions
as appropriate to the generalized setup defined.

## Installation

Run gentoo-install.sh with appropriate parameters. An example of this for NVME-based SSD, and Encryption with Hibernation:

```
sudo ./gentoo-install.sh /dev/nvme0n1 -e -s
```

This will start the provisioning setup, with the full expectations that the system is ready to go as-is, as it will format. You will get one prompt before it actually starts.

## Stage Installation

After the preparation phase, it completes and allows you time to verify what's happened and
go over any issues you may or may not see, before you continue on to stage installation.

The Stage installation, using `--stage <stage>` allows you to setup stage3 or stage4 based on
what you provide for <stage>, which can be a url or a file to an appropriate stagefile.

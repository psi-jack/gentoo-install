#!/bin/bash

bhost="$(hostname -s)"
bdate="$(date +"%Y%m%d")"

if [[ ! -d "/var/backup" ]]; then
    mkdir /var/backup
fi

pushd /var/backup &>/dev/null || exit 2

echo "Creating stage4 backup in /var/backup/$bhost-stage4.zst"
mkstage4 -s -l -q -C zst \
    -e "/var/lib/containers/*" \
    -e "/var/lib/machines/*" \
    -e "/var/lib/portables/*" \
    -e "/var/lib/libvirt/*" \
    -e "/var/cache/binpkgs/*" \
    -e "/var/backup/*" \
    -e "/home/*/*" \
    "$bhost-stage4-$bdate"

if [[ "$?" -ne 0  ]]; then
    echo "ERROR: Backup failure"
    exit 1
else
    if [[ -f "/var/backup/$bhost-stage4.tar.zst" ]]; then
        rm -f "/var/backup/$bhost-stage4.tar.zst"
    fi
    mv "/var/backup/$bhost-stage4-$bdate.tar.zst" "/var/backup/$bhost-stage4.tar.zst"
fi

popd &>/dev/null || exit 2
